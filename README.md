# Indonesia

Materi keamanan digital dalam bahasa Indonesia

Website
1. [Security in the box](https://securityinabox.org/id/) {+quite old+} - 
2. [Modul perlindungan digital](https://advokasi.aji.or.id/safety)
4. Keamanan jurnalis {+engagemedia+} - [1](https://engagemedia.org/2017/persepsi-tentang-keamanan-dan-keselamatan-profesi-wartawan-di-filipina/)
| [2](https://engagemedia.org/2017/apakah-profesi-wartawan-aman-di-indonesia/)
5.. [Pedoman keamanan dari Indonesialeaks](https://www.indonesialeaks.id/pedoman-keamanan.html)

PDF
1. [Penelitian Finfisher di Indonesia](https://citizenlab.ca/docs/finfisher-indonesia.pdf)
2. [Panduan Siber Center for Digital Society](https://literasidigital.id/books/siberpedia-panduan-pintar-keamanan-siber-2/)
3. [Panduan Keamanan Kampanye Digital NDI](https://www.ndi.org/sites/default/files/Cybersecurity%20for%20Campaigns%20Playbook%20-%20Indonesian.pdf)
4. [Buku Saku KBGO](https://bit.ly/BukuSakuKBGO)
5. [Panduan Informasi dan Tips Komponen untuk Keamanan dan Privasi](https://www.combine.or.id/archive/61) from Combine {+translated from Tactical Tech+} 
6. [Panduan Informasi dan Tips Keamanan dan Privasi Dunia Digital](https://www.combine.or.id/archive/62) from Combine
7. [Edisi 70 : Perlindungan Data Pribadi](https://www.combine.or.id/archive/21) from Combine
8. [Budi Rahardjo, Keamanan Informasi](http://budi.rahardjo.id/files/keamanan.pdf)
9. [Pentingnya kemitraan untuk keamanan digital](http://onnocenter.or.id/pustaka/White%20Paper%20TikTok%20-%2019%20Okt2020%20-%20%20Pentingnya%20Kemitraan%20Untuk%20Memperkuat%20Keamanan%20Siber%20Indonesia.pdf) {+CFDS, Onno Center+}

Video
1. [Internet Sehat serial siber](https://internetsehat.id/cybersecurity/)
2. [Keamanan Digital untuk Jurnalis (Webinar) dari pulitzercenter.org](https://www.youtube.com/watch?v=XYKYdAY7H_Y&feature=emb_logo)

Jurnalis
1. [Panduan keamanan jurnalis dari Facebook](https://id-id.facebook.com/business/help/252441975867823)

Aplikasi
1. [TOR Browser bahasa Indonesia](https://tb-manual.torproject.org/id/)
2. [Panduan Signal Bhs Indo - Localizationlab](https://static1.squarespace.com/static/542b02d7e4b0a92b527f5750/t/5c0806ae03ce649b420345ae/1544029870911/Bahasa+Indonesia+-+Signal+for+Android.pdf)

Riset
